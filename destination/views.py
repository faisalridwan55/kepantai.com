from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import Beach, Photos, Review, MapCollection
from django.core import serializers
import json, ast

# Create your views here.
response = {}
def destination_search_page(request):
    if 'category_flag' in request.session:
        html = 'destination_search_page.html'
        del request.session['category_flag']
        return render(request, html, response)
    else:
        list_beach = Beach.objects.all()
        response['list_beach'] = list_beach
        html = 'destination_search_page.html'
        return render(request, html, response)

def beach_details(request, id):
    beach = Beach.objects.get(pk=id)
    beach_name = beach.name
    print(beach_name)
    list_photo = Photos.objects.filter(beach_name=beach_name)
    print(list_photo)
    review = Review.objects.get(beach_name=beach_name)
    print(review)
    response['review'] = review
    response['list_photo'] = list_photo
    response['beach'] = beach
    html = 'beach_details.html'
    return render(request, html, response)

def get_beach_name(request):
    data = Beach.objects.all()
    return HttpResponse(serializers.serialize('json', data), content_type="application/json")

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

def getInformation(request, id):
    beach = Beach.objects.get(pk=id)
    information = beach.location_description
    beach = model_to_dict(beach)
    beach = ast.literal_eval(beach)
    return JsonResponse(beach)

def get_beach(request, beach_name):
    beach = Beach.objects.get(name=beach_name)
    # print(beach.pk)
    beach = model_to_dict(beach)
    beach = ast.literal_eval(beach)
    return JsonResponse(beach)

def get_beach_by_name(request, nama_pantai):
    if nama_pantai == "all":
        beach = Beach.objects.all()
        response['list_beach'] = beach
        response['single_beach'] = 0
        if 'single_beach' in request.session:
            del request.session['single_beach']
        return redirect(reverse('destination:destination-search-page'))
    else:
        beach = Beach.objects.get(name=nama_pantai)
        response['single_beach'] = 1
        response['beach'] = beach
        # request.session['flag'] = "single"
        return redirect(reverse('destination:destination-search-page'))

def get_beach_by_category(request, kategori_pantai):
    beach = Beach.objects.filter(beach_category__contains=kategori_pantai)
    # for i in beach:
    #     print(i.name)
    if beach.count()>1:
        response['list_beach'] = beach
        print(response['list_beach'])
        request.session['category_flag'] = 1
        response['single_beach'] = 0
        # if 'single_beach' in request.session:
        #     del request.session['single_beach']
        return redirect(reverse('destination:destination-search-page'))
    else:
        response['beach'] = beach.first()
        response['single_beach'] = 1
        return redirect(reverse('destination:destination-search-page'))
    # # print(beach)
    # if 'single_beach' in request.session:
    #     del request.session['single_beach']

    # if beach.count()>1:
    #     print("banyak")
    #     response['single_beach']=0
    #     response['list_beach'] = beach
    # else :
    #     print("satu")
    #     response['single_beach']=0
    #     response['beach'] = beach
    # return redirect(reverse('destination:destination-search-page'))
    #
def get_beach_map(request, id, tipe, name):
    # beach_target = Beach.objects.get(pk=id)
    # beach_name = beach_target.name
    beach = MapCollection.objects.get(beach_name=name, tipe=tipe)
    print("tipe " + tipe)
    print("beach " + beach.url)
    # print("test")
    # url = url.url
    return HttpResponse(beach.url)
