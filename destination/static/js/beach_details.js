$(document).ready(function(){
  // Description
  $("#location-a").click(function(){
    getInformation("location");
  });
  $("#activities-a").click(function(){
    getInformation("activities");
  });
  $("#event-and-festival-a").click(function(){
    getInformation("event");
  });
  $("#price-a").click(function(){
    getInformation("price");
  });
  var getInformation = function(type) {
    $.ajax({
        method: "GET",
        url: "get/beach/information/",
        success : function (data) {
          if (type == "location") {
            $('#content').text(data['location_description']);
          }else if (type == "activities") {
            $('#content').text(data['activities']);
          }else if (type == "event") {
            $('#content').text(data['event_and_festival']);
          }else if (type == "price") {
            $('#content').text(data['price']);
          }
        }
    });
  }

  var location = JSON.parse(dataPantai);
  var beachName =  $('#beach-name-hidden').text();

  var latitude = parseFloat(location[beachName].latitude);
  var longitude = parseFloat(location[beachName].longitude);

  
  // Map information
  $("#route-a").click(function(){
    initMap(latitude,longitude);
    console.log("route");
  });
  $("#transportation-a").click(function(){
    getMap("transportation");
    console.log("transportation");
  });
  $("#souvenirs-a").click(function(){
    getMap("souvenirs");
    console.log("souvenirs");
  });
  $("#restaurant-a").click(function(){
    getMap("restaurant");
    console.log("restaurant");
  });
  $("#accomodation-a").click(function(){
    getMap("accomodation");
    console.log("accomodation");
  });
  $("#real-time-a").click(function(){
    // $('#map').html(
    //   "<h1 id='real-time-status'>Condition: Not Crowded</h1>"
    // );
    getMap("realtime");
    console.log("real_time");
  });

  var getMap = function (tipe) {
    beach_name = $('#beach-name-hidden').text();
    console.log(beach_name);
    $.ajax({
        method: "GET",
        url: "get/beach/map/" + tipe + "/" + beach_name + "/",
        success : function (data) {
          $('#map').html(
            data
          );
        }
    });
  }
});
