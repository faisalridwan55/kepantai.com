function initMap(latitude,longitude) {

  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;

  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -7.78278, lng: 110.36083},
    zoom: 13  
  });

  calculateAndDisplayRoute(latitude,longitude,directionsService, directionsDisplay);
  directionsDisplay.setMap(map);
  
}

function calculateAndDisplayRoute(latitude,longitude,directionsService, directionsDisplay) {          
  try {
    directionsService.route({ 
      origin: {lat: -7.7955798, lng: 110.36948959999995 },
      destination: {lat: latitude, lng: longitude }, 
      travelMode: google.maps.TravelMode['DRIVING']
    }, function(response, status) {
      if (status == 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });  
  }catch(e){
    window.alert(e);
  }            
}