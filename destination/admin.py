from django.contrib import admin

# Register your models here.
from destination.models import Beach, Review, Photos, MapCollection

admin.site.register(Beach)
admin.site.register(Review)
# admin.site.register(Category)
admin.site.register(Photos)
admin.site.register(MapCollection)
