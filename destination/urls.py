from django.conf.urls import url
from .views import destination_search_page, beach_details, get_beach_name, get_beach, getInformation, get_beach_by_name, get_beach_map, get_beach_by_category

urlpatterns = [
    url(r'^$', destination_search_page, name='destination-search-page'),
    url(r'^get/beach/name/', get_beach_name, name='get-beach-name'),
    url(r'^(?P<id>\d)/get/beach/map/(?P<tipe>.*)/(?P<name>.*)/', get_beach_map, name='get-beach-map'),
    url(r'^get/(?P<beach_name>.*)/information/', get_beach, name='get-beach'),
    url(r'^(?P<id>\d)/get/beach/information/', getInformation, name='get-information'),
    url(r'^(?P<id>\d)/', beach_details, name='beach-details'),
    url(r'^get/(?P<nama_pantai>.*)/', get_beach_by_name, name='get-beach-by-name'),
    url(r'^find/category/(?P<kategori_pantai>.*)/', get_beach_by_category, name='get-beach-by-category'),
]
