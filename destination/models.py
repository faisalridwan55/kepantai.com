from django.db import models

# Create your models here.

class Beach(models.Model):
    name = models.CharField(max_length=140, blank=False)

    description = models.TextField()
    location_description = models.TextField()
    activities = models.TextField()
    event_and_festival = models.TextField()
    price = models.TextField()
    beach_category = models.TextField()
    frame_location = models.CharField(max_length=140, blank=False)

    display_picture = models.CharField(max_length=140, blank=False)
    cover_picture = models.CharField(max_length=140, blank=False)
    class Meta:
        ordering = ('name', 'id')

class Review(models.Model):
    user_name = models.CharField(max_length=140, blank=False)
    user_from = models.CharField(max_length=140, blank=False)
    beach_name = models.CharField(max_length=140, blank=False)
    content = models.TextField()
    rating = models.CharField(max_length=140, blank=False)
    picture = models.CharField(max_length=140, blank=False)

    profile_picture = models.CharField(max_length=140, blank=False)

# class Category(models.Model):
#     category_name = models.CharField(max_length=140, blank=False)
#     member = models.ForeignKey(Beach)

class MapCollection(models.Model):
    beach_name = models.CharField(max_length=140, blank=False)
    tipe = models.CharField(max_length=140, blank=False)
    url = models.CharField(max_length=140, blank=False)

class Photos(models.Model):
    beach_name = models.CharField(max_length=140, blank=False)
    picture = models.CharField(max_length=140, blank=False)
