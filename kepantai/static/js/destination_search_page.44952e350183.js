$(document).ready(function(){
  $.ajax({
      method: "GET",
      url: "get/beach/name/",
      success : function (data) {
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            var nama = data[i]['fields']['name'];
            // console.log(nama);
            var option = $("<option value="+ nama +"></option>").text(nama);
            $('#beach-name-selection').append(option);
          }
        }
      }
  });

  $('#beach-name-selection').change(function(){
    console.log($(this).val());
    var nama_pantai = $(this).val();
    window.open("get/"+nama_pantai+"/", '_self');
  });


  // TOLONG IMPLEMENT INI KINUUUUUUUUUUUUU
  $('#beach-category-selection').change(function(){
    // console.log($(this).val());
    var kategori_pantai = $(this).val();
    window.open("find/category/"+kategori_pantai+"/", '_self');
  });
});
