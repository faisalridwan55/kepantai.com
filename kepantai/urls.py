"""kepantai URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import destination.urls as destination
import transportation.urls as transportation
import home.urls as home
import review.urls as review
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^destination/', include(destination,namespace='destination')),
    url(r'^transportation/', include(transportation,namespace='transportation')),
    url(r'^home/', include(home,namespace='home')),
    url(r'^review/', include(review,namespace='review')),
    url(r'^$', RedirectView.as_view(permanent=True, url='/home/'), name='index'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
