from django.conf.urls import url
from .views import transportation_page,map_util

urlpatterns = [
    url(r'^$', transportation_page, name='transportation_page'),
    url(r'util/$', map_util, name='map_util'),
]
