from django.shortcuts import render, redirect, reverse
from django.http import (HttpResponse, HttpResponseRedirect)

# Create your views here.

response = {}
def transportation_page(request):
	html = 'transportation.html'
	return render(request, html, response)

def map_util(request):
	html = 'util.html'
	return render(request, html , response)
